using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;

class binaryClockView extends Ui.WatchFace {

	var offscreenBuffer;
	var curClip;
	var fullScreenRefresh;

	var debug;
	
	// layout settings 
    var row_1 = 165;
    var row_2 = 135;
    var row_3 = 105;
    var row_4 = 75;
    
    var col_1 = 45;
    var col_2 = 75;
    var col_3 = 105;
    var col_4 = 135;
    var col_5 = 165;
    var col_6 = 195;
    
    var dotSize = 10;

    function initialize() {
        WatchFace.initialize();
        fullScreenRefresh = true;
        debug = false;
    }

    // Load your resources here
    function onLayout(dc) {
        //setLayout(Rez.Layouts.WatchFace(dc));
    		offscreenBuffer = new Graphics.BufferedBitmap({
	        :width=>dc.getWidth(),
	        :height=>dc.getHeight(),
	        :palette=> [
	            Graphics.COLOR_DK_GRAY,
	            Graphics.COLOR_LT_GRAY,
	            Graphics.COLOR_BLACK,
	            Graphics.COLOR_WHITE,
	            Graphics.COLOR_BLUE,
	            Graphics.COLOR_DK_BLUE
	        ]
	    });
	    
	    curClip = null;
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        var width;
        var height;
        var screenWidth = dc.getWidth();
        var clockTime = System.getClockTime();
        var targetDc = null;

        // We always want to refresh the full screen when we get a regular onUpdate call.
        fullScreenRefresh = true;

        dc.clearClip();
        curClip = null;
        // If we have an offscreen buffer that we are using to draw the background,
        // set the draw context of that buffer as our target.
        targetDc = offscreenBuffer.getDc();
        
        
        width = targetDc.getWidth();
        height = targetDc.getHeight();

        // Fill the entire background with Black.
        targetDc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_WHITE);
        targetDc.fillRectangle(0, 0, dc.getWidth(), dc.getHeight());
        
        // draw hours
        var currentHour = clockTime.hour;
        drawHours(targetDc, currentHour);
        
        // draw minutes
        var currentMinute = clockTime.min;
		drawMinutes(targetDc, currentMinute);
        
		// show clipping area
		if (debug) {
			targetDc.setColor(Graphics.COLOR_RED, Graphics.COLOR_RED);
			targetDc.drawRectangle(col_5 - dotSize, row_4 - dotSize, 51, 111);
		}
		
        // draw to screen
        drawBackground(dc);
        
        // draw seconds
		onPartialUpdate(dc);
		
        fullScreenRefresh = false;
    }
    
    // Handle the partial update event
    function onPartialUpdate( dc ) {
        // If we're not doing a full screen refresh we need to re-draw the background
        // before drawing the updated second hand position. Note this will only re-draw
        // the background in the area specified by the previously computed clipping region.
        if (!fullScreenRefresh) {
            drawBackground(dc);
        }

        var clockTime = System.getClockTime();
        var currentSecond = clockTime.sec;
        
        // define clip to prevent redrawal of the entire screen
        dc.setClip(col_5 - dotSize, row_4 - dotSize, 51, 111);

        drawSeconds(dc, currentSecond);
    }
    
    function drawHours(dc, currentHour) {
    		var currentHour10 = Math.floor(currentHour / 10);
        var currentHour1 = currentHour % 10;

        setActiveColor(dc, isBitActive(currentHour10, 0));
        dc.fillCircle(col_1, row_1, dotSize); // bit 0 (1)
        setActiveColor(dc, isBitActive(currentHour10, 1));
        dc.fillCircle(col_1, row_2, dotSize); // bit 1 (2)
        
        setActiveColor(dc, isBitActive(currentHour1, 0));
        dc.fillCircle(col_2, row_1, dotSize); // bit 0 (1)
        setActiveColor(dc, isBitActive(currentHour1, 1));
        dc.fillCircle(col_2, row_2, dotSize); // bit 1 (2)
        setActiveColor(dc, isBitActive(currentHour1, 2));
        dc.fillCircle(col_2, row_3, dotSize); // bit 2 (4)
        setActiveColor(dc, isBitActive(currentHour1, 3));
        dc.fillCircle(col_2, row_4, dotSize); // bit 3 (8)
    }
    
    function drawMinutes(dc, currentMinute) {
        var currentMinute10 = Math.floor(currentMinute / 10);
        var currentMinute1 = currentMinute % 10;
        
        setActiveColor(dc, isBitActive(currentMinute10, 0));
        dc.fillCircle(col_3, row_1, dotSize); // bit 0 (1)
        setActiveColor(dc, isBitActive(currentMinute10, 1));
        dc.fillCircle(col_3, row_2, dotSize); // bit 1 (2)
        setActiveColor(dc, isBitActive(currentMinute10, 2));
        dc.fillCircle(col_3, row_3, dotSize); // bit 2 (4)
        
        setActiveColor(dc, isBitActive(currentMinute1, 0));
        dc.fillCircle(col_4, row_1, dotSize); // bit 0 (1)
        setActiveColor(dc, isBitActive(currentMinute1, 1));
        dc.fillCircle(col_4, row_2, dotSize); // bit 1 (2)
        setActiveColor(dc, isBitActive(currentMinute1, 2));
        dc.fillCircle(col_4, row_3, dotSize); // bit 2 (4)
        setActiveColor(dc, isBitActive(currentMinute1, 3));
        dc.fillCircle(col_4, row_4, dotSize); // bit 3 (8)
    }
    
    function drawSeconds(dc, currentSecond) {
        var currentSecond10 = Math.floor(currentSecond / 10);
        var currentSecond1 = currentSecond % 10;
        
        setActiveColor(dc, isBitActive(currentSecond10, 0));
        dc.fillCircle(col_5, row_1, dotSize); // bit 0 (1)
        setActiveColor(dc, isBitActive(currentSecond10, 1));
        dc.fillCircle(col_5, row_2, dotSize); // bit 1 (2)
        setActiveColor(dc, isBitActive(currentSecond10, 2));
        dc.fillCircle(col_5, row_3, dotSize); // bit 2 (4)
        
        setActiveColor(dc, isBitActive(currentSecond1, 0));
        dc.fillCircle(col_6, row_1, dotSize); // bit 0 (1)
        setActiveColor(dc, isBitActive(currentSecond1, 1));
        dc.fillCircle(col_6, row_2, dotSize); // bit 1 (2)
        setActiveColor(dc, isBitActive(currentSecond1, 2));
        dc.fillCircle(col_6, row_3, dotSize); // bit 2 (4)
        setActiveColor(dc, isBitActive(currentSecond1, 3));
        dc.fillCircle(col_6, row_4, dotSize); // bit 3 (8)
    }
    
    // draw the screen buffer 
    function drawBackground(dc) {
    		dc.drawBitmap(0, 0, offscreenBuffer);
    }
    
    // check if the specified bit is active in the given number
    function isBitActive(number, bit) {
    		return number & (1 << bit);
    }
    
    function setActiveColor(dc, isActive) {
        if (isActive) {
        		dc.setColor(Graphics.COLOR_BLUE, Graphics.COLOR_DK_BLUE);
        } else {
        		dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_DK_GRAY);
        }
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // This method is called when the device re-enters sleep mode.
    function onEnterSleep() {
        WatchUi.requestUpdate();
    }

    // This method is called when the device exits sleep mode.
    function onExitSleep() {
    }

}
